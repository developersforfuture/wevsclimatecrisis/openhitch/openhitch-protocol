This repository is part of project [OpenHitch](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/blob/main/README.md)
It defines the protocol between app and server by defining a bunch of data structures and an [HTTP-interface](src/main/java/de/wifaz/oh/protocol/HitchInterface.kt)

