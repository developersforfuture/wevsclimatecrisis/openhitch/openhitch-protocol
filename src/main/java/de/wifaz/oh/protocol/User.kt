package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class User @JsonCreator constructor(
    @JsonProperty("id") val id: String,
    @JsonProperty("first_name") val first_name: String,
    @JsonProperty("last_name") val last_name: String,
    @JsonProperty("nick_name") val nick_name: String? = null,
    @JsonProperty("phone") val phone: String? = null,
    @JsonProperty("email") val email: String? = null
) : java.io.Serializable
