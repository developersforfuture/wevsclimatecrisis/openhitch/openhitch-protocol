package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

class LiftInfoMessage @JsonCreator constructor(
    @JsonProperty("reference_way_id") reference_way_id: String,
    @JsonProperty("lift_infos") val lift_infos: ArrayList<LiftInfo>
) : HitchMessage(reference_way_id), java.io.Serializable
