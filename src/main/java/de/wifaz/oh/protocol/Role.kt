package de.wifaz.oh.protocol

enum class Role : java.io.Serializable {
    DRIVER,
    PASSENGER
}
