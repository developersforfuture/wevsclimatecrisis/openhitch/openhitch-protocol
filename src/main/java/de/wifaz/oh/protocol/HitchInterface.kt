package de.wifaz.oh.protocol

import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface HitchInterface {
    @POST("users/create")
    fun createUser(@Body data: User): Call<CreateUserResponse>

    @PUT("user/{id}")
    fun updateUser(@Path("id") id: String, @Body data: User): Call<Unit>

    @PUT("user/{id}/profilePicture")
    fun uploadProfilePicture(@Path("id") id: String, @Body profilePicture: RequestBody): Call<RegistrationState>

    @GET("user/{id}/profilePicture")
    fun getProfilePicture(@Path("id") id: String): Call<ResponseBody>

    @PUT("feedbacks/create")
    fun createFeedback(@Body data: Feedback): Call<Unit>

    @PUT("ways/create")
    fun createWay(@Body data: Way): Call<Unit>

    @DELETE("way/{id}")
    fun deleteWay(@Path("id") id: String): Call<Unit>

    // FIXME should be PATCH
    @PUT("lift/{id}/status")
    fun updateLiftStatus(@Path("id") id: String, @Body data: Lift.Status): Call<Unit>

    // FIXME should be PATCH
    @PUT("way/{id}/status")
    fun updateWayStatus(@Path("id") id: String, @Body data: Way.Status): Call<Unit>

    @GET("syncData/{id}")
    fun getSyncData(@Path("id") id: String): Call<SyncData>

    @GET("geocodeSearch")
    fun geocodeSearch(
        @Query("q") query: String,
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("lang") lang: String,
        @Query("limit") limit: Int = 5
    ): Call<List<OHPoint>>

    @GET("geocodeReverseSearch")
    fun geocodeReverseSearch(
        @Query("lat") lat: Double,
        @Query("lon") lon: Double,
        @Query("lang") lang: String,
        @Query("limit") limit: Int = 5
    ): Call<List<OHPoint>>

    companion object {
        const val MAX_OFFERS = 10
    }

}
