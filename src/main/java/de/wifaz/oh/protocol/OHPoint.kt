package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class OHPoint @JsonCreator constructor(
    @JsonProperty("latitude") val latitude: Double,
    @JsonProperty("longitude") val longitude: Double
) : java.io.Serializable {

    var description: String? = null

    fun withDescription(description: String): OHPoint {
        this.description = description
        return this
    }

    override fun toString(): String {
        return description ?: String.format("(%.5f,%.5f)", latitude, longitude)
    }

    companion object {
        fun fromLngLat(longitude: Double, latitude: Double): OHPoint {
            return OHPoint(latitude, longitude)
        }
    }
}
