package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

/**
 * Complete data set for a user on the server.
 * Used for synchronizing clients.
 */
class SyncData @JsonCreator constructor(
    @JsonProperty("user") val user: User,
    @JsonProperty("ways") val ways: ArrayList<Way>,
    @JsonProperty("lift_infos") val lift_infos: ArrayList<LiftInfo>
) : java.io.Serializable