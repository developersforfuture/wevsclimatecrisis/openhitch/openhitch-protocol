package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

data class RegistrationState @JsonCreator constructor(
    @JsonProperty("user_id") val user_id: String,
    @JsonProperty("has_profile_picture") val has_profile_picture: Boolean,
    @JsonProperty("phone_verified") val phone_verified: Boolean,
    @JsonProperty("fully_registered") val fully_registered: Boolean,
) : java.io.Serializable