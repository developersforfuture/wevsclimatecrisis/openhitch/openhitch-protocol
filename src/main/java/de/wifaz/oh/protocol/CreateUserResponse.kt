package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty


data class CreateUserResponse @JsonCreator constructor(
    @JsonProperty("token") val token: String,
    @JsonProperty("registration_state") val registration_state: RegistrationState
)