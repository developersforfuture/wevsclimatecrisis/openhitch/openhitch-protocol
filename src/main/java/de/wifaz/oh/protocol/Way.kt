package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.util.*

data class Way @JsonCreator constructor(
    @JsonProperty("id") val id: String,
    @JsonProperty("status") val status: Status,
    @JsonProperty("user_id") val user_id: String,
    @JsonProperty("role") val role: Role,
    @JsonProperty("waypoints") val waypoints: ArrayList<OHPoint>,

    /**
     * Start time of trip (driver) or start of time window (passenger) in
     * milliseconds since 1/1/1970
     */
    @JsonProperty("start_time") val start_time: Long,

    /**
     * End time of time window in milliseconds since 1/1/1970
     */
    @JsonProperty("end_time") val end_time: Long,

    @JsonProperty("seats") val seats: Int,
    @JsonProperty("autocommit") val autocommit: Boolean,

    /**
     * Maximum time in milliseconds that the driver is willing to spend on detours to
     * pick up or drop off passengers. This field is yet unused for passenger ways.
     * It defaults to DEFAULT_DRIVER_MAX_DETOUR_TIME for driver ways.
     */
    @JsonProperty("max_detour_time") val max_detour_time: Long? = null,

    /**
     * When set false (or null) on a passenger way, only drivers will be searched, that can bring the
     * passenger directly to their destination within the detour limits. When true, also
     * matches are searched, where the driver can bring the passenger a reasonable section of the way.
     *
     * For driver ways, this property has no effect and should be set to null.
     */
    @JsonProperty("sectional_match") val sectional_match: Boolean? = null
) : java.io.Serializable {
    enum class Status {
        PRELIMINARY,  // client internal use only
        RESEARCH,  // searching on server for potential partners without publishing
        PUBLISHED,
        STARTED,
        FINISHED,
        CANCELED
    }

    companion object {
        val collisionRelevantStates = EnumSet.of(Status.PUBLISHED, Status.STARTED)
        const val DEFAULT_DRIVER_MAX_DETOUR_TIME = 5 * 60 * 1000L
    }
}
