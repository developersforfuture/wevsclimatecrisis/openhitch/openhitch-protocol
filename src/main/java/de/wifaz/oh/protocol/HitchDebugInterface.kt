package de.wifaz.oh.protocol

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface HitchDebugInterface {
    @GET("personas")
    fun getPersonas(): Call<List<User>>
}