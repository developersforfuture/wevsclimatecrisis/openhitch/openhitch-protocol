package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class WayStatusMessage @JsonCreator constructor(
    @JsonProperty("reference_way_id") val way_id: String,
    @JsonProperty("status") val status: Way.Status

) : HitchMessage(way_id), java.io.Serializable
