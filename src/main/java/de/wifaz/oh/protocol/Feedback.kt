package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty

@JsonIgnoreProperties(ignoreUnknown = true)
data class Feedback @JsonCreator constructor(
    @JsonProperty("id") val id: String,
    @JsonProperty("content") val content: String,
    @JsonProperty("user_id") val user_id: String
) : java.io.Serializable
