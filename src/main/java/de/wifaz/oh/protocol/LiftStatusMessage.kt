package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty

class LiftStatusMessage @JsonCreator constructor(
    @JsonProperty("reference_way_id") reference_way_id: String,
    @JsonProperty("lift_id") val lift_id: String,
    @JsonProperty("status") val status: Lift.Status

) : HitchMessage(reference_way_id), java.io.Serializable
