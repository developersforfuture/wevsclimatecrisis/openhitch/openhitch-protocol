package de.wifaz.oh.protocol

import com.fasterxml.jackson.annotation.JsonCreator
import com.fasterxml.jackson.annotation.JsonProperty
import java.math.BigDecimal
import java.util.*

data class Lift @JsonCreator constructor(
    @JsonProperty("id") val id: String,
    @JsonProperty("status") val status: Lift.Status,
    @JsonProperty("passenger_way_id") val passenger_way_id: String,
    @JsonProperty("driver_way_id") val driver_way_id: String,
    @JsonProperty("pick_up_point") val pick_up_point: OHPoint,
    @JsonProperty("pick_up_hint") val pick_up_hint: BigDecimal,
    @JsonProperty("drop_off_point") val drop_off_point: OHPoint,
    @JsonProperty("drop_off_hint") val drop_off_hint: BigDecimal,
    @JsonProperty("rating") val rating: Double,
    @JsonProperty("shared_distance") val shared_distance: Double,
    @JsonProperty("pickup_time") val pickup_time: Long,
    @JsonProperty("drop_off_time") val drop_off_time: Long,
    @JsonProperty("price") val price: BigDecimal,
    @JsonProperty("currency") val currency: String,
    @JsonProperty("detour_distance") val detour_distance: Double,
    @JsonProperty("detour_time") val detour_time: Long,
    @JsonProperty("sectional") val sectional: Boolean
) : java.io.Serializable {
    enum class Status {
        SUGGESTED,
        REQUESTED,
        WITHDRAWN,
        ACCEPTED,
        PREVIEW,
        REJECTED,
        BOARDED,
        FINISHED,
        DRIVER_CANCELED,
        PASSENGER_CANCELED
    }

    companion object {
        val engagedStates = EnumSet.of(Status.ACCEPTED, Status.BOARDED, Status.FINISHED)
    }

}
